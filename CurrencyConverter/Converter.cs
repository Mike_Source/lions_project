﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConverter
{
    public class Converter
    {
        double value = 0.0;
        double rate = 0.0;


        //---un-select either lines below to use the ExchangeRate or the ExcahngeRateDatabase--//
        ExchangeRate rateChecker = new ExchangeRate();
        //ExchangeRateDatabase rateChecker = new ExchangeRateDatabase();


        public double Convert(double amount, string currency)
        {
            double total = 0.0;
            
            value = amount;

            //---un-select either lines below to use the ExchangeRate or the ExcahngeRateDatabase--//
            rate = rateChecker.getExchangeRate("rates.xml", currency);
            //rate = rateChecker.RetrieveXMLDocument(currency);


            total = value * rate; 

            return total;
        }

        public double Convert(double amount, string currency1, string currency2)
        {
            double total = 0.0;

            //value = amount;

            //---un-select either lines below to use the ExchangeRate or the ExcahngeRateDatabase--//
            double rate1 = rateChecker.getExchangeRate("rates.xml", currency1);
            //rate = rateChecker.RetrieveXMLDocument(currency1);
            double rate2 = rateChecker.getExchangeRate("rates.xml", currency2);
            //rate = rateChecker.RetrieveXMLDocument(currency2);

            double factor = rate2 / rate1;

            total = factor * amount;

            return Math.Round(total, 2);
        }
    }
}
