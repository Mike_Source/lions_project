﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;

namespace CurrencyConverter
{
    public class ExchangeRate
    {
        double rateValue;
                
        public double getExchangeRate(string path, string currency)
        {

            List<string> rate = new List<string>();
            List<double> rate_val = new List<double>();
            List<string> ticker_symb = new List<string>();
            
            StreamReader xmlRead = new StreamReader(path);
            StringBuilder output = new StringBuilder();

           do
            {
               output.Append(xmlRead.ReadLine());

            } while (xmlRead.Peek() != -1);

            string[] extract = output.ToString().Split('\'');

            //------------------------------------------//
            //getting list of tickers and rates in string format

            for (int i = 3; i < extract.Length; i += 2)
            {
                rate.Add(extract[i]);
            }

         //------2 lists creation: tickerSymbol and rate value lists   

            //aim: get 2 full list in both <string and double>
            for (int i = 0; i < rate.Count; i+=2)
			{
                
                ticker_symb.Add(rate[i]);
                		 
			}

            for (int i = 1; i < rate.Count; i+= 2)
            {
                Double.TryParse(rate[i], out rateValue);
                rate_val.Add(rateValue);
            }

        // --------------------------------------------------------------//


           //merging 2 lists(ticker_sym and rate_val) to form dictonary erates//
            var eRates = ticker_symb.Zip(rate_val, (t, r) => new { ticker_symb = t, rate_val = r })
                         .ToDictionary(x => x.ticker_symb, x => x.rate_val);

            return eRates[currency.ToUpper()]; 
        }
    }
}
