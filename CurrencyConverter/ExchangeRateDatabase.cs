﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CurrencyConverter
{
    public class ExchangeRateDatabase
    {
        Currencies currencies = new Currencies();

        public double RetrieveXMLDocument(string symbol)
        {
            XDocument ratesData = XDocument.Load("rates.xml");

            var cubes = from cube in ratesData.Descendants("Cube")
                        select cube;

            foreach (var cube in cubes)
            {
                string Symbol = cube.Attribute("currency").Value.ToString();
                if (Symbol == symbol.ToUpper()) 
                {
                    currencies.symbol = Symbol;
                    currencies.currencyRate = Convert.ToDouble(cube.Attribute("rate").Value);
                    break;
                }
                
               
            }
                        
            return currencies.currencyRate;
        }
        
       
    }
}
