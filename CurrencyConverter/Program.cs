﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            
            double amountVal;
            string input = "";

            Console.WriteLine("PLEASE NOTE: Type \"end\" when you're done converting\n\n");

            do
            {
                Converter converter = new Converter();

                /// -------------------------------/// -- old
                /*
                Console.WriteLine("\n enter currency to convert to: ");
                string currency = Console.ReadLine();

                Console.WriteLine("enter amount to be converted: ");
                string amountStr = Console.ReadLine();
                */
                /// -------------------------------/// - old

                Console.WriteLine("\nenter amount to be converted: ");
                string amount = Console.ReadLine();

                Console.WriteLine("\nenter currency to convert from: ");
                string currencyFrom = Console.ReadLine();

                Console.WriteLine("\nenter currency to convert to: ");
                string currencyTo = Console.ReadLine();

                

                Double.TryParse(amount, out amountVal);

                try
                {
                    Console.WriteLine("Amount is {0} " + "{1}", converter.Convert(amountVal, currencyFrom.ToUpper(), currencyTo.ToUpper()), currencyTo.ToUpper());
                }
                catch (Exception exception)
                {

                    Console.WriteLine(exception.Message); ;
                }

                Console.Write("\nMore conversions? Type \"end\" to end the program \n or press enter to make another conversion:  ");
                input = Console.ReadLine().ToLower();
                
            } while (input != "end");

                        
            Console.ReadLine();
        }
    }
}
