﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CurrencyConverter;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test_Convert_Return0_WhenGivenNoAmount()
        {
            //arrange
            Converter convert = new Converter();
            double amount = 0.0;
            string currency = "GBP";
            //act
            double convertedAmount = convert.Convert(amount, currency);
            //assert
            Assert.AreEqual(0, convertedAmount);
        }

        #region RedundantTest
        //[TestMethod]
        //public void Test_Convert_Returns1_()
        //{
        //    //arrange
        //    Converter convert = new Converter();
        //    double amount = 1.0;
        //    double exchangeRate = 1.0;
        //    //act
        //    double convertedAmount = convert.Convert(amount, exchangeRate);
        //    //assert
        //    Assert.AreEqual(1.0, convertedAmount);
        //} 
        #endregion

        #region RedundantTest2
        //[TestMethod]
        //public void Test_GetAmount_ReturnProduct()
        //{

        //    //arrange
        //    Converter convert = new Converter();
        //    double amount = 1.0;
        //    double exchangeRate = 1.1002;
        //    //act
        //    double convertedAmount = convert.Convert(amount, exchangeRate);

        //    //assert
        //    Assert.AreEqual(1.1002, convertedAmount);

        //} 
        #endregion

        [TestMethod]
        public void Test_getExchangeRate_ReturnExchangeRate()
        {

            //arrange
            ExchangeRate test = new ExchangeRate();

            //Dictionary<string, double> output = new Dictionary<string, double>();

            //act
            //double convertedAmount = convert.Convert(amount, exchangeRate);


            double output = test.getExchangeRate("rates.xml", "JPY");

            //assert
            Assert.AreEqual(122.86, output);

        }

        [TestMethod]
        public void Test_Convert_ReturnConversion_WhenGivenAmountAndCurrency()
        {
            //arrange
            double amount = 100;
            string destCurrency = "CHF";
            Converter test = new Converter();

            //act
            double convertedAmount = test.Convert(amount, destCurrency);

            //assert
            Assert.AreEqual(109.21, convertedAmount, 2);


        }

        [TestMethod]
        public void Test_RetrieveXMLDocument_ReturnExchangeRate_WhenGivenCurrency()
        {
            //arrange
            ExchangeRateDatabase database = new ExchangeRateDatabase();
            string symbol = "GBP";
            //act
            double amount = database.RetrieveXMLDocument(symbol);

            //assert
            Assert.AreEqual(0.78935, amount);
        }
    }
}
