﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConverter
{
    public class Currencies
    {
        public string symbol { get; set; }
        public double currencyRate { get; set; }

    }
}
